import {Game, PLAYER1} from './game';
import './assets/morpion.css';

/**
 * Interface graphique pour le jeu de morpion
 */
export class Board {
    /**
     * Affiche un plateau de jeu interactif dans l’élément spécifié.
     *
     * @param root  Élément racine
     */
    constructor(root) {
        this.root = root;

        const board = document.createElement('div');
        board.classList.add('board');
        this.root.appendChild(board);

        // On crée un `div` qui représente la grille de jeu et qui contiendra les cases
        const grid = document.createElement('div');
        grid.classList.add('grid');
        board.appendChild(grid);

        // On crée les cases et les ajoutes à la grille
        this.cells = [];
        for (let i = 0; i < 9; i++) {
            const cell = document.createElement('div');
            cell.classList.add('cell');

            const symbol = document.createElement('div');
            symbol.classList.add('symbol');
            cell.appendChild(symbol);

            this.cells.push(cell);
            grid.appendChild(cell);
        }

        // On crée une nouvelle partie logique
        this.morpion = new Game();

        // On associe les callback aux cliques sur nos cellules
        for (let index of this.cells.keys()) {
            this.cells[index].onclick = () => this.onClickCell(index);
        }

        // On ajoute 2 inputs pour les noms des joueurs
        this.nameInput1 = document.createElement('input');
        this.nameInput1.type = 'text';
        this.nameInput1.placeholder = 'Joueur 1';
        this.root.appendChild(this.nameInput1);

        this.nameInput2 = document.createElement('input');
        this.nameInput2.type = 'text';
        this.nameInput2.placeholder = 'Joueur 2';
        this.root.appendChild(this.nameInput2);

        // On ajoute un bouton "reset"
        const button = document.createElement('button');
        button.classList.add('reset');
        button.append('Reset');
        button.onclick = () => this.reset();
        this.root.appendChild(button)
    }

    /**
     * Met en évidence les cases indiquées.
     *
     * @param indexes   Liste des cases à mettre en évidence
     */
    highlight(indexes) {
        for (let index of indexes) {
            this.cells[index].classList.add('win');
        }
    }

    /**
     * Gère les cliques des joueurs sur les cases.
     *
     * @param index         Indice de la case cliquée
     */
    onClickCell(index) {
        const cell = this.cells[index];

        // Si le coup n’est pas possible, on quitte la fonction sans rien faire de plus.
        if (!this.morpion.move(index)) {
            return;
        }

        // On indique que le joueur courant à jouer un coup dans la case cliquée
        // en ajoutant la classe CSS adéquate.
        if (this.morpion.currentPlayer === PLAYER1) {
            cell.classList.add('player1');
        } else {
            cell.classList.add('player2');
        }

        let winPattern = this.morpion.checkWin();
        if (!winPattern) {
            // On indique que c’est l’autre joueur qui joue à présent.
            this.morpion.switchPlayer();

            if (this.morpion.isTerminated()) {
                window.alert('Match nul !');
            }
        } else {
            this.highlight(winPattern);

            // On récupère l’input du gagnant
            let nameInput;
            if (this.morpion.currentPlayer === PLAYER1) {
                nameInput = this.nameInput1;
            } else {
                nameInput = this.nameInput2;
            }

            // On récupère son nom si il est défini, sinon le placeholder
            const name = nameInput.value || nameInput.placeholder;
            window.alert(`${name} a gagné !`);

            this.morpion.terminate();
        }
    }

    /**
     * Réinitialise l’état de la partie et l’affichage.
     */
    reset() {
        for (let cell of this.cells) {
            cell.classList.remove('player1', 'player2', 'win');
        }
        this.morpion = new Game();
    }
}
